var express = require("express");
var router = express.Router();
var { Escala, Materia, Teacher } = require("../models");
var { Op } = require("sequelize");
const moment = require("moment");


function week(current = new Date()) {
  var week = new Array();
  // Starting Monday not Sunday
  current.setDate(current.getDate() - current.getDay() + 1);
  for (var i = 0; i < 7; i++) {
    week.push(new Date(current));
    current.setDate(current.getDate() + 1);
  }
  return week;
}
/* GET home page. */
router.get("/", async function (req, res, next) {
  const materias = await Materia.findAll();
  const professores = await Teacher.findAll();
  const escalas = await Escala.findAll({
    include: [
      { model: Teacher, as: "professor" },
      { model: Materia, as: "materia" },
    ],
    order: [["date", "ASC"]],
  });
  const semanal = await Escala.findAll({
    include: [
      { model: Teacher, as: "professor" },
      { model: Materia, as: "materia" },
    ],
    where: {
      date: { 
        [Op.between]: [week()[0], week()[6]]
      }
    },

  });
  const Mensal = await Escala.findAll({
    include: [
      { model: Teacher, as: "professor" },
      { model: Materia, as: "materia" },
    ],
    where: {
      date: {
        [Op.between]: [
          new Date(new Date().getFullYear(), new Date().getMonth(), 1),
          new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
        ],
      },
    },
    })
  


  res.render("index", {
    moment: require("moment"),
    start_date: moment(Object.values(escalas)[0]?.date).format("YYYY-MM-DD"),
    end_date: moment(Object.values(escalas).reverse()[0]?.date).format("YYYY-MM-DD"),
    week: week(),
    title: "Gerador de escala",
    semanal,
    Mensal,
    escalas,
    professores,
    materias,
    ...req.query
  });
});

module.exports = router;
