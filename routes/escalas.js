var express = require("express");
var router = express.Router();
var { Escala, Teacher, Materia } = require("../models");
var { Op } = require("sequelize");
const moment = require("moment");

function getDateOfWeek(w, y) {
  var d = 1 + (w - 1) * 7; // 1st of January + 7 days for each week

  return new Date(y, 0, d);
}

function week(current = new Date()) {
  var week = new Array();
  // Starting Monday not Sunday
  current.setDate(current.getDate() - current.getDay() + 1);
  for (var i = 0; i < 7; i++) {
    week.push(new Date(current));
    current.setDate(current.getDate() + 1);
  }
  return week;
}

router.get("/", async function (req, res, next) {
  const materias = await Materia.findAll();
  const professores = await Teacher.findAll();
  const escalas = await Escala.findAll();
  res.render("index", { title: "Escalas", escalas, professores, materias });
});

router.get("/semana", async function (req, res, next) {
  const { semana } = req.query;
  const year = semana.split("-")[0];
  const year_week = parseInt(semana.split("W")[1]) + 1;
  const currentWeek = week(getDateOfWeek(year_week, year));
  console.log(currentWeek);
  const escalas = await Escala.findAll({
    include: [
      { model: Teacher, as: "professor" },
      { model: Materia, as: "materia" },
    ],
  });
  const semanal = await Escala.findAll({
    include: [
      { model: Teacher, as: "professor" },
      { model: Materia, as: "materia" },
    ],
    where: {
      date: {
        [Op.between]: [currentWeek[0], currentWeek[6]],
      },
    },
  });

  res.render("_partial/semana", {
    week: currentWeek,
    moment: require("moment"),
    semanal,
    escalas,
    ...req.query,
  });
});

router.get("/lista", async function (req, res, next) {
  const escalas = await Escala.findAll({
    include: [
      { model: Teacher, as: "professor" },
      { model: Materia, as: "materia" },
    ],
  });

  res.render("_partial/lista", {
    moment: require("moment"),
    week: week(),
    escalas,
    materias,
    ...req.query,
  });
});

router.all("/delete/:id", async function (req, res, next) {
  const { id } = req.params;
  const escala = await Escala.findByPk(id);
  await escala.destroy();
  res.redirect("/");
});

router.post("/create", async function (req, res, next) {
  const { teacher_id, materia_id, data } = req.body;
  console.log(req.body);
  console.log();
  if (
    await Escala.findOne({
      where: {
        [Op.and]: [
          { teacher_id },
          {
            date: {
              [Op.between]: [
                new Date(data + " 00:00:00 -03:00"),
                new Date(data + " 23:59:59 -03:00"),
              ],
            },
          },
        ],
      },
    })
  ) {
    return res.redirect("/?error=busy");
  }
  if (
    await Escala.findOne({
      where: {
        [Op.and]: [
          { materia_id },
          {
            date: {
              [Op.between]: [
                new Date(data + " 00:00:00 -03:00"),
                new Date(data + " 23:59:59 -03:00"),
              ],
            },
          },
        ],
      },
    })
  ) {
    return res.redirect("/?error=class");
  }
  console.log(data);
  await Escala.create({
    teacher_id,
    materia_id,
    date: new Date(data + " 00:00:00 -03:00"),
  });
  res.redirect("/");
});

router.all("/week/csv", async function (req, res, next) {
  const { semana } = req.query;
  const year = semana.split("-")[0];
  const year_week = parseInt(semana.split("W")[1]) + 1;
  const currentWeek = week(getDateOfWeek(year_week, year));
  const params = {
    order: [["date", "ASC"]],
    include: [
      {
        model: Teacher,
        as: "professor",
      },
      {
        model: Materia,
        as: "materia",
      },
    ],
  };

  params.where = {
    [Op.and]: [
      {
        date: {
          [Op.between]: [currentWeek[0], currentWeek[6]],
        },
      },
    ],
  };

  const escalas = await Escala.findAll(params);

  res.setHeader("Content-Type", "text/csv");
  res.setHeader("Content-Disposition", "attachment; filename=escalas.csv");
  currentWeek.forEach((day) => {
    res.write(`\n${moment(day).format("dd/MM")},`);
    escalas.forEach((escala) => {
      if (moment(escala.date).isSame(moment(day), "day")) {
        res.write(
          `${escala.materia.name} - ${escala.professor.role} ${escala.professor.name} `
        );
      }
    });
  });
  res.end();
});
router.all("/csv", async function (req, res, next) {
  const { start_date, end_date } = req.query;
  const params = {
    order: [["date", "ASC"]],
    include: [
      {
        model: Teacher,
        as: "professor",
      },
      {
        model: Materia,
        as: "materia",
      },
    ],
  };
  if (start_date && end_date) {
    params.where = {
      [Op.and]: [
        {
          date: {
            [Op.between]: [
              new Date(start_date),
              new Date(end_date + " 23:59:59"),
            ],
          },
        },
      ],
    };
  }
  const escalas = await Escala.findAll(params);

  res.setHeader("Content-Type", "text/csv");
  res.setHeader("Content-Disposition", "attachment; filename=escalas.csv");
  res.write("Professor,Materia,Data\n");
  escalas.forEach((escala) => {
    res.write(
      `${escala.professor.role} ${escala.professor.name},${
        escala.materia.name
      },${moment(escala.date).format("DD/MM/YYYY")}\n`
    );
  });
  res.end();
});

module.exports = router;
