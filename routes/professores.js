var express = require('express');
var router = express.Router();
var { Teacher, Escala }  = require('../models');
var { Op } = require('sequelize');
/* GET users listing. */
router.get('/', async function(req, res, next) {
  const teacher = await Teacher.findAll();
  console.log(req.query)
  res.render('professores/index', { title: 'Professores', teachers: teacher, ...req.query });
});
router.get('/new', async function(req, res, next) {
  res.render('professores/new', { title: 'Professores'});
});

router.get('/:id', async function(req, res, next) {
  const teacher = await Teacher.findByPk(req.params.id);
  res.render('professores/edit', { title: 'Professores', teacher: teacher, ...req.query });
});


router.post('/create', async function(req, res, next) {
  console.log(req.body);
  const { name, role } = req.body;
  console.log(await Teacher.findAll({where:{name, role}}))
  if ((await Teacher.count({where:{name, role}}))){
      return res.redirect('/professores?error=duplicate');

  }
  const teacher = await Teacher.create({ name, role });
  res.redirect('/professores');
});

router.post('/update/:id', async function(req, res, next) {
  const { name, role } = req.body;
  const { id } = req.params;
if ((await Teacher.count({where:{[Op.and]:[{name, role}, {id: {[Op.ne]: id}}]}}))){
      return res.redirect('/professores/'+id+'?error=duplicate');

  }
  const teacher = await Teacher.update({ name, role }, { where: { id } });
  res.redirect('/professores');
});

router.all('/:id/delete/', async function(req, res, next) {
  const { id } = req.params;
  const teacher = await Teacher.findByPk(id);
  const escala = await Escala.findAll({where:{teacher_id: id}});
  await Promise.all(Object.values(escala).map(async e => e.destroy()));
  await teacher.destroy();
  res.redirect('/professores');
});


module.exports = router;
