var express = require('express');
var router = express.Router();
var { Materia,Escala }  = require('../models');

router.get('/', async function(req, res, next) {
  const materias = await Materia.findAll();
  res.render('materias/index', { title: 'Materia', materias, ...req.query });
});

router.post('/create', async function(req, res, next) {
  const { name } = req.body;
  if(await Materia.findOne({where:{name}})){
    return res.redirect('/materias?error=duplicate');
}
  const materia = await Materia.create({ name });
  res.redirect('/materias');
});

router.all('/delete/:id', async function(req, res, next) {
  const { id } = req.params;
  const materia = await Materia.findByPk(id);
  const escala = await Escala.findAll({where:{materia_id: id}});
  await Promise.all(Object.values(escala).map(async e => e.destroy()));
  await materia.destroy();
  res.redirect('/materias');
});

module.exports = router;