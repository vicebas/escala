function updateLista() {
  const data = $("#lista-form").serialize();
  $.ajax({
    type: "GET",
    url: "/escalas/lista",
    data: data,
    success: function (data) {
      $("#listadiv").html(data);
    },
  });
}
function updateSemana() {
    $("#semanadiv").html('loading...');
  const data = $("#lista-form").serialize();
  $.ajax({
    type: "GET",
    url: "/escalas/semana",
    data: data,
    success: function (data) {
      $("#semanadiv").html(data);
    },
  });
}
