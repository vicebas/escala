'use strict';
const {
  Model
} = require('sequelize');
const Teacher = require('./Teachers');
const Materia = require('./Classes');

module.exports = (sequelize, DataTypes) => {
  class Escala extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Escala.init({
    date: DataTypes.DATE,
    teacher_id: DataTypes.INTEGER,
    materia_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Escala',
    tableName: 'escala'
  });
  Escala.belongsTo(Teacher(sequelize,DataTypes), { as: 'professor',foreignKey:'teacher_id', constraints: false })
  Escala.belongsTo(Materia(sequelize,DataTypes), { as: 'materia',foreignKey:'materia_id', constraints: false })

  return Escala;
};