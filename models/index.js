'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const Umzug = require('umzug');
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
const Teacher = require(__dirname + '/Teachers');
const Materia = require(__dirname + '/Classes');
const Escala = require(__dirname + '/Escala');


let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config);
}

const umzug = new Umzug({
        storage: 'sequelize',
        storageOptions: {
            sequelize: sequelize
        },

        // see: https://github.com/sequelize/umzug/issues/17
        migrations: {
            params: [
                sequelize.getQueryInterface(), // queryInterface
                sequelize.constructor, // DataTypes
                function () {
                    throw new Error('Migration tried to use old style "done" callback. Please upgrade to "umzug" and return a promise instead.');
                }
            ],
            path: './migrations',
            pattern: /\.js$/
        },

        logging: function () {
            console.log.apply(null, arguments);
        }
    });

    function logUmzugEvent(eventName) {
        return function (name, migration) {
            console.log(`${name} ${eventName}`);
        }
    }
    function runMigrations() {
        return umzug.up();
    }

    umzug.on('migrating', logUmzugEvent('migrating'));
    umzug.on('migrated', logUmzugEvent('migrated'));
    umzug.on('reverting', logUmzugEvent('reverting'));
    umzug.on('reverted', logUmzugEvent('reverted'));

    runMigrations();

module.exports = {
  sequelize,
  Sequelize: sequelize,
  Teacher: Teacher(sequelize,Sequelize.DataTypes),
  Materia: Materia(sequelize,Sequelize.DataTypes),
  Escala: Escala(sequelize,Sequelize.DataTypes)
}


